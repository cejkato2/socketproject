#include <config.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <err.h>
#include <errno.h>
#include "hexdump.h"

struct hexdump *hx;

int opensocket(const char *dev)
{
    int sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, "wlo1", 5);
    if (sd < 0) {
        err(1, "Failed to bind to device.");
    }
    return sd;
}

int packet_raw(int sd)
{
    struct sockaddr saddr;
    int saddr_size = sizeof(saddr), data_size;

#define BUFFERSIZE 65535
    uint8_t buffer[BUFFERSIZE + 1];
    uint8_t hexbuffer[BUFFERSIZE + 1];

    data_size = recvfrom(sd, buffer, BUFFERSIZE, 0, &saddr, (socklen_t*) &saddr_size);
    if (data_size < 0) {
        err(1, "Failed to receive packet.");
    }

    hxd_write(hx, buffer, data_size);
    hxd_flush(hx);
    data_size = hxd_read(hx, hexbuffer, BUFFERSIZE);
    printf("%.*s", data_size, hexbuffer);
}

int main(int argc, char **argv)
{
    const char *dev = NULL;
    int i, c, listdevs = 0;
    int sd;

    /* handle command line arguments */
    while (1) {
        int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
            {"listdevs",  no_argument, 0,  'l' },
            {"device",    required_argument, 0,  'd' },
            {"help",    no_argument, 0,  'h' },
            {0,         0,                 0,  0 }
        };

        c = getopt_long(argc, argv, "d:hl", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 'd':
            dev = optarg;
            break;
        case 'h':
            printf("%s\n", PACKAGE_STRING);
            printf("Usage: %s [-dfhl]\n", PACKAGE_NAME);
            printf("\t-d, --device DEVICE\tCapture from network interface card\n");
            printf("\t-l, --listdevs\tPrint list of available devices (for -d argument)\n");
            exit(EXIT_SUCCESS);
        case 'l':
            listdevs = 1;
            break;
        case '?':
            break;

        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc) {
        /* unused positional arguments */
        printf("non-option ARGV-elements: ");
        while (optind < argc)
            printf("%s ", argv[optind++]);
        printf("\n");
    }

    if (listdevs == 1) {
        exit(EXIT_SUCCESS);
    }

    sd = opensocket(dev);
    
    hx = hxd_open(NULL);
    hxd_compile(hx, HEXDUMP_C, HXD_NETWORK);

    for (i=0; i<10; i++) {
	    printf("%d. socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))\n", i + 1);
	    packet_raw(sd);
    }

    hxd_close(hx);
    close(sd);
}

