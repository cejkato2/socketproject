# General information

This example shows packet retrieval using standard socket set to `PF_PACKET`.

# Compilation

`autoreconf -i && ./configure -q && make`

# Execution

`sudo ./socketproject -d eth0`

# Example output:

```
$ sudo ./socketproject
socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))
00000000  d8 58 d7 00 0e df d4 3b  04 6d 31 2f 08 00 45 00  |.X.....;.m1/..E.|
00000010  00 3f c0 8b 40 00 40 11  f5 d8 c0 a8 01 f8 c0 a8  |.?..@.@.........|
00000020  01 01 b3 59 00 35 00 2b  84 86 63 e5 01 00 00 01  |...Y.5.+..c.....|
00000030  00 00 00 00 00 00 0d 66  65 64 6f 72 61 70 72 6f  |.......fedorapro|
00000040  6a 65 63 74 03 6f 72 67  00 00 01 00 01           |ject.org.....|
...
00000140  0c 00 01 00 01 00 00 00  3c 00 04 43 db 90 44     |........<..C..D|
```

# Credits

hexdump code was taken from https://github.com/wahern/hexdump/

